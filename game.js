var SampleState = {
    preload: function(){

    },
    create: function(){

    },
    update: function(){

    }
};

var TestState = {
    preload: function(){
        game.load.image('background', 'Assets/background.png');
       // game.load.image('ground', 'Assets/ground.png');

        game.load.spritesheet('player', 'Assets/Plane.png', 50, 20);

    },
    create: function(){
        game.stage.backgroundColor = '#3498db';
        this.background = game.add.image(0, 0, 'background'); 

        this.background.height = game.height;
        this.background.width = game.width;

        game.physics.startSystem(Phaser.Physics.ARCADE);

        game.renderer.renderSession.roundPixels = true;

        this.cursor = game.input.keyboard.createCursorKeys();

        this.player = game.add.sprite(game.width/2, game.height/2, 'player');
        
        this.player.anchor.setTo(0.5,0.5);
        this.player.isjumping = false;
        this.player.isdashing = false;
        this.player.facing = 1; //facing is basically equal to 'this.player.scale.x', I just too lazy to change it.

        this.player.animations.add('run',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17], 18, true);
        this.player.animations.add('idle',[18,19,20,21], 2, true);
        this.player.animations.add('jump_up',[22,23], 18, true);
        this.player.animations.add('jump_mid',[24,25,26], 18, false);
        this.player.animations.add('jump_down',[27,28], 18, true);
        this.player.animations.add('dash',[29,30,31,32,33,34], 12, false);

        this.floor = game.add.sprite(0, game.height - 30, 'ground'); 
        this.floor.width = game.width;
        game.physics.arcade.enable(this.floor);
        this.floor.body.immovable = true;

        game.physics.arcade.enable(this.player);
        this.player.body.gravity.y = 500;
        this.player.body.collideWorldBounds = true; //prevent player from falling the edge of screen.

    },
    update: function(){
        game.physics.arcade.collide(this.player, this.floor);
        if (!this.player.inWorld)
            game.state.start('Test');
        this.PlayerControl();
    },
    PlayerControl: function(){

        var direction = 0;

        var z_key = game.input.keyboard.addKey(Phaser.Keyboard.Z);

        if (this.cursor.left.isDown){
            if(this.player.facing > 0) 
            {
                this.player.scale.x *=-1;
                this.player.facing = -1;
            }
            if(!this.cursor.right.isDown)
                direction = -1;
            else
                direction = 0;
            if(!this.player.isjumping && !this.player.isdashing)
                this.player.animations.play('run');
        }

        if (this.cursor.right.isDown){
            if(this.player.facing < 0) 
            {
                this.player.scale.x *=-1;
                this.player.facing = 1;
            }
            if(!this.cursor.left.isDown)
                direction = 1;
            else
                direction = 0;
            if(!this.player.isjumping && !this.player.isdashing)
                this.player.animations.play('run');
        }

        if (this.cursor.up.isDown){
            if(this.player.body.touching.down){
                this.player.body.velocity.y = -350;
            }
        }

        if(this.player.body.touching.down)
        {
            if(this.player.body.velocity.y < 0)
                this.player.isjumping = true;
            else
                this.player.isjumping = false;
        }
        else
        {
            if(!this.player.isjumping)
                this.player.animations.play('jump_down');
            else
            {
                if(this.player.body.velocity.y < -50)
                {
                    if(!this.player.isdashing)
                        this.player.animations.play('jump_up');
                }
                else if(this.player.body.velocity.y >= -50 && this.player.body.velocity.y < 50)
                {
                    this.player.animations.stop('jump_up');
                    if(!this.player.isdashing)
                        this.player.animations.play('jump_mid');
                }
                else
                {
                    if(!this.player.isdashing)
                        this.player.animations.play('jump_down');
                }
            }
        }

        if(z_key.isDown)
        {
            if(!this.player.isdashing)
            {
                this.player.animations.stop();
                this.player.body.velocity.x += 800*this.player.scale.x;
                this.player.isdashing = true;
                this.player.animations.play('dash');
            }
        }

        if(this.player.body.velocity.x >= -200 && this.player.body.velocity.x <= 200)
        {
            this.player.isdashing = false;
            this.player.body.velocity.x = 200*direction;
        }
        else
        {
            this.player.body.velocity.x *= 0.95;
        }
            
        if(this.player.body.velocity.x == 0 && this.player.body.touching.down)
        {
            this.player.animations.play('idle');
        }
    }
};

var game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas');

game.state.add('Test', TestState);
game.state.start('Test');