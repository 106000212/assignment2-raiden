var loadState ={
    preload:function(){
        var loadingLabel = game.add.text(game.width/2,150,'loading...',{font:'40px Arial',fill:'#ffffff'});
        loadingLabel.anchor.setTo(0.5,0.5);

        var progressBar = game.add.sprite(game.width/2,200,'progressBar');
        progressBar.anchor.setTo(0.5,0.5);
        game.load.setPreloadSprite(progressBar);
        game.load.image('background1', 'ASS/background1.png');

    },
    create:function(){
        game.state.start('menu');
    },
}